package com.example.apiaomg.data.remote

import com.example.apiaomg.data.News
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsAPIService {
    @GET("/v2/top-headlines")
    suspend fun getAllNews(
        @Query("country") iso639:String = "fr",
        @Query("apiKey") apiKey:String = "c9be387af6c847a3b36c6de26b239bde"
    ): News
}