package com.example.apiaomg.data

import com.example.apiaomg.data.remote.NewsRemoteDataSource

class NewsRepository(private  val newsDataSource: NewsRemoteDataSource){
    suspend fun getNews() = newsDataSource.getNews()
}